# testino

testino  is a check port application.

## Requeriments
- Python 3.8 
- [Buildpack](https://buildpacks.io) for docker image creation.  

## Run application

Install python libs
```sh
pip3 install -r requirements.txt
```

Set environments variables:

Port for Flask applicaiton
```sh
export PORT=8080
```
Hosts filename
```sh
export HOSTS_FILE=hosts.yaml
```
Config flask app
```sh
export FLASK_APP="app.py"
```

Run applicaiton
```sh
flask run
```



## Buildpack

Install and documentation for [Buildpack](https://buildpacks.io/docs/)

List builders 
```sh
pack builder suggest
```

Select default-builder(example: Google for builder)
```sh
pack config default-builder gcr.io/buildpacks/builder:v1 
```

Create image
```sh
pack build testino:001
```


## Run application with docker

```sh
docker run -d -ePORT=8080 -eHOSTS_FILE=hosts.yaml -p8080:8080 --name testino testino:1
```

### Hosts file
Yaml file to config hosts to check
```sh
gitlab:
  host: www.gitlab.com
  port: 443

google:
  host: www.googsssle.es
  port: 80

```

