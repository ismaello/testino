from flask import Flask
from flask import render_template
import yaml
import socket
import time
import os




def checkConnection(location):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try: 
        response=sock.connect_ex(location)
        sock.close()
    except:
        print(" connection error")
        response=-1
    return('OK' if response == 0 else 'Error')

def loadConfigFile(fileName):
    with open("hosts.yaml") as file:
        hosts= yaml.load(file, Loader=yaml.FullLoader)
    return(hosts)

app = Flask(__name__)



@app.route("/")
@app.route("/status")
def index():
    hosts=None
    
    if os.environ.get('HOSTS_FILE') is not None:
        hosts=loadConfigFile("hosts.yaml")
        for k,v in hosts.items():
            location=(v['host'],v['port'])
            hosts[k]['check']=checkConnection(location)
        return render_template("index.html", data= hosts)
    else:
        return render_template("error.html")